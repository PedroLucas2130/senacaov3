﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacao.Views
{
    
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroUsuarioView : ContentPage
    {
        public Usuario Usuario { get; set; }
        public CadastroUsuarioView()
        {
            InitializeComponent();
            this.Usuario = new Usuario();

            this.BindingContext = this;
        }
        
        private void Button_Clicked(object sender, EventArgs e)
        {

        }
    }
}